/**
 * Created by Alexander on 09-06-2016.
 */
function goServices () {

    window.location = "/services";
    console.log(window.location );
}
$(document).ready(function(){
    var description = $('#description').val();
    $("ServiceTable").text($(this).text().replace(description, '<b>' + description + '<\b>'));
    $("#ServiceTable td").each(function() {
        var html = $(this).html().replace(description, '<b class="bg-warning">' + description + '</b>');
        $(this).html(html);
    });

    <!-- Dialog show event handler -->
    $('#confirmDelete').on('show.bs.modal', function (e) {
        console.log('show.bs.modal');
        var message = $(e.relatedTarget).attr('data-message');
        $(this).find('.modal-body p').text(message);
        var title = $(e.relatedTarget).attr('data-title');
        $(this).find('.modal-title').text(title);

        // Pass form reference to modal for submission on yes/ok
        var form = $(e.relatedTarget).closest('form');
        $(this).find('.modal-footer #confirm').data('form', form);
    });

    <!-- Form confirm (yes/ok) handler, submits form -->
    $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
        $('#form-actions').submit();
    });
});