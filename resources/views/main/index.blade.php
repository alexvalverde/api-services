@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @include('...main.partials.filter')
                @include('...main.partials.table')
            </div>
        </div>
    </div>
@endsection
