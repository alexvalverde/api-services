<div class="panel panel-default">
    <div class="panel-heading">Services</div>
    <div class="panel-body">
        @if( count($services) > 0 )
            <div class="table-responsive">
                <table class="table table-striped" id="ServiceTable">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 20%">Name</th>
                        <th class="text-center" style="width: 70%">Description</th>
                        <th class="text-center" style="width: 10%">Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                        <tr>
                            <td class="text-left">{{ $service->name }}</td>
                            <td class="text-left">
                                <div class="col-md-12 no-padder">
                                    <div>
                                        {{ $service->description }}
                                    </div>
                                    <div class="col-md-12 text-left">
                                        <div class="col-md-4 text-info">
                                            <b class="text-info">Email</b> : {{ isset($service->user) ?  $service->user->email : ''  }}
                                        </div>
                                        <div class="col-md-4 text-info">
                                            <b>Phone :</b> {{ isset($service->user->profile ) ?  $service->user->profile->phone: '' }}
                                        </div>
                                        <div class="col-md-4 text-info">
                                            <b>Cellphone :</b>{{ isset($service->user->profile ) ? $service->user->profile->cellphone : ''}}
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="text-right">${{ $service->price }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th class="text-center">Name</th>
                        <th class="text-center">Description</th>
                        <th class="text-center">Price</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-md-12 text-center">
                <div class="pagination"> {!! $services->appends([
                                                'name' => $params['name'],
                                                 'min' => $params['min'],
                                                 'description' => $params['description'],
                                                 'max' => $params['max']
                                            ])->render() !!}
                </div>
            </div>
        @else
            <div>
                <h3>No results found!</h3>
            </div>
        @endif
    </div>
</div>