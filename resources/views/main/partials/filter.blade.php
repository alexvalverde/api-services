<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="panel-heading">
            <div class="col-md-11 no-padder">
                <form action="{{ route('services') }}" class="form-inline">
                    <div class="col-md-2 form-group">
                        <input type="text" name="name" class="form-control" style="width: 100%" placeholder="Name Service" value="{{$params['name']}}">
                    </div>
                    <div class="col-md-5 form-group">
                        <input type="text" id="description" name="description" class="form-control" style="width: 100%" placeholder="Description" value="{{ $params['description'] }}">
                    </div>
                    <div class="col-md-2 form-group">
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="number" name="min" class="form-control" style="width: 100%" placeholder="Min Price" value="{{ $params['min'] }}">
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="number" name="max" class="form-control" style="width: 100%" placeholder="Max Price" value="{{ $params['max'] }}">
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <input type="submit" class="text-center btn btn-success" value="Send">
                    </div>
                </form>
            </div>
            <div class="col-md-1 no-padder">
                <div class="form-group">
                    <button class="text-center btn btn-primary">
                        <span class="glyphicon glyphicon-refresh" aria-hidden="true" onclick="goServices()"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</nav>
