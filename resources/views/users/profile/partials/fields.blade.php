<input class="form-control" name="user_id" type="hidden" value="{{ $profile->user_id }}"/>
<div class="form-group">
    First Name
    <input class="form-control" name="first_name" type="text" placeholder="First Name" value="{{ $profile->first_name }}"/>
</div>
<div class="form-group">
    Last Name
    <input class="form-control" name="last_name" type="text" placeholder="Last Name" value="{{  $profile->last_name }}"/>
</div>
<div class="form-group">
    Phone
    <input class="form-control" name="phone" type="text" placeholder="Phone" value="{{ $profile->phone }}"/>
</div>
<div class="form-group">
    Cellphone
    <input class="form-control" name="cellphone" type="text" placeholder="Cellphone" value="{{ $profile->cellphone }}"/>
</div>
<div class="form-group">
    Zip Code
    <input class="form-control" name="zip_code" type="text" placeholder="Zip Code" value="{{ $profile->zip_code }}"/>
</div>
<div class="form-group">
    Address
    <input class="form-control" name="address" type="text" placeholder="Address" value="{{  $profile->address }}"/>
</div>