<div class="panel panel-default">
    <div class="panel-heading">Profile</div>
    <div class="panel-body">
        <h1>{{ $profile->first_name . ' ' . $profile->last_name  }}</h1>
        <div class="col-lg-9">
            <div class="field-group col-lg-12 no-padder">
                <label class="col-lg-3">Zip Code:</label>
                <div class="col-lg-9">{{ $profile->zip_code }}</div>
            </div>
            <div class="field-group  col-lg-12 no-padder">
                <label class="col-lg-3">Address:</label>
                <div class="col-lg-9">{{ $profile->address }}</div>
            </div>
             <div class="field-group col-lg-12 no-padder">
                 <label class="col-lg-3">Cell Phone</label>
                 <div class="col-lg-9">{{ $profile->cellphone }}</div>
             </div>
             <div class="field-group col-lg-12 no-padder">
                  <label class="col-lg-3">Phone:</label>
                  <div class="col-lg-9">{{ $profile->phone }}</div>
              </div>
        </div>
        <div class="col-lg-3">
            <div class="image-profile">
                <img class="image-profile" src="{{ asset( 'images/profile/' . $profile->picture ) }}" alt=""/>
            </div>
        </div>
    </div>
</div>