@extends('......layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ route('dashboard.profiles.store') }}" method="POST">
                {{ csrf_field() }}
                @include('users.profile.partials.fields')
                <button type="submit" class="btn btn-success btn-block">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection