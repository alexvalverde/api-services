@extends('......layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('users.profile.partials.navbar')
            @include('users.profile.partials.content')
        </div>
    </div>
</div>
@endsection