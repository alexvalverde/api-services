@extends('......layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form-horizontal" action="{{ route('dashboard.profiles.update') }}  " method="POST">
                {{ method_field('PUT') }}
                {{ csrf_field() }}

                @include('users.profile.partials.fields')
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 no-padder">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-success btn-block" href="{{ url('dashboard/users/'. Auth::user()->id . '/profiles') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-success btn-block">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection