@extends('......layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    @include('...services.partials.delete_confirm')
                    <div class="panel-heading">Services</div>
                        <div class="panel-body">
                            <div class="service-item col-md-12">
                                <div class="service-attribute col-md-3">ID</div><div class="col-md-9">{{ $service->id }}</div>
                            </div>
                            <div class="service-item col-md-12">
                                <div class="service-attribute col-md-3">Name</div><div class="col-md-9">{{ $service->name }}</div>
                            </div>
                            <div class="service-item col-md-12">
                                <div class="service-attribute col-md-3">Description</div><div class="col-md-9">{{ $service->description }}</div>
                            </div>
                            <div class="service-item col-md-12">
                                <div class="service-attribute col-md-3">Price</div><div class="col-md-9">{{ $service->price }}</div>
                            </div>
                        </div>
                    <div class="panel-footer">
                        <div class="text-right">
                            <form id="form-actions" action="{{ route('dashboard.users.services.destroy',['users' => $service->user_id, 'services' => $service->id])}}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <td class="text-center">
                                    <a href="{{ url('dashboard/users/' .$service->user_id . '/services') }}" class="btn btn-info btn-xs">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                    </a>
                                    <button class="btn btn-xs btn-danger" type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Delete User" data-message="Are you sure you want to delete this user ?">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </button>
                                    <a href="{{ url('dashboard/users/' .$service->user_id . '/services/' . $service->id.'/edit') }}" class="btn btn-primary btn-xs">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection