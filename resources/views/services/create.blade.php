@extends('......layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ route('dashboard.users.services.store', ['users' => $service->user_id]) }}" method="POST">
             {{ csrf_field() }}
                @include('...services.partials.fields')
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 no-padder">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-success btn-block" href="{{ url('dashboard/users/'. Auth::user()->id . '/services') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-success btn-block">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection