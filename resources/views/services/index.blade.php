@extends('......layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('...services.partials.navbar')
            @include('...services.partials.table')
        </div>
    </div>
</div>
@endsection