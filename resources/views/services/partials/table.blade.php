<div class="panel panel-default">
    @include('...services.partials.delete_confirm')
    <div class="panel-heading">Services</div>
    <div class="panel-body">
    @if(count($services) > 0)
        <table class="table table-bordered table-striped" id="ServicesTable">
          <thead>
            <tr>
                <th class="text-center" style="width: 60px">ID</th>
                <th class="text-center" style="width: 120px">Name</th>
                <th class="text-center">Description</th>
                <th class="text-center" style="width: 100px">Price</th>
                <th class="text-center" style="width: 120px">Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($services as $service)
                <tr>
                    <td class="text-center">{{ $service->id }}</td>
                    <td class="text-left">{{ $service->name }}</td>
                    <td class="text-justify">{{ $service->description }}</td>
                    <td class="text-right">{{ $service->price }}</td>
                <form id="form-actions" action="{{ route('dashboard.users.services.destroy',['users' => $service->user_id, 'services' => $service->id])}}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <td class="text-center">
                        <button class="btn btn-xs btn-danger" type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Delete Service" data-message="Are you sure you want to delete this service?">
                            <i class="glyphicon glyphicon-trash"></i>
                        </button>
                        <a href="{{ url('dashboard/users/' .$service->user_id . '/services/' . $service->id.'/edit') }}" class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </a>
                        <a href="{{ url('dashboard/users/' .$service->user_id . '/services/' . $service->id) }}" class="btn btn-info btn-xs">
                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                        </a>
                    </td>
                </form>
                </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
                <th class="text-center">ID</th>
                <th class="text-center">Name</th>
                <th class="text-center">Description</th>
                <th class="text-center">Price</th>
                <th class="text-center">Actions</th>
            </tr>
          </tfoot>
        </table>
        @else
            <div>
                <h3>
                    You have no Services
                </h3>
            </div>
        @endif
    </div>
</div>