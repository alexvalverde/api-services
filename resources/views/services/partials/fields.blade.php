<div class="form-group">
    Name Service
    <input class="form-control" name="name" type="text" placeholder="Name Service" value="{{ $service->name }}"/>
</div>
<div class="form-group">
    Category
    <select class="form-control" name="category_id" type="text" placeholder="Category" value="{{  $service->category_id  }}">
        @if(!$service->category_id)
            <option selected> Choose Category </option>
        @endif
        @foreach($categories as $category)
            @if($service->category_id === $category->id)
                 <option selected value="{{ $category->id }}"> {{ $category->name }} </option>
            @else
                <option value="{{ $category->id }}"> {{ $category->name }} </option>
            @endif
        @endforeach
    </select>
</div>
<div class="form-group">
    Description
    <input class="form-control" name="description" type="text" placeholder="Description" value="{{ $service->description  }}"/>
</div>
<div class="form-group">
    Price
    <input class="form-control" name="price" min="1" step="0.01" type="number" placeholder="Price" value="{{ $service->price}}"/>
</div>

<input class="form-control" name="user_id" type="hidden" value="{{ $service->user_id }}"/>