<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceTag extends Model
{
    protected $table = 'service_tags';

    protected $fillable = ['service_id', 'tag_id'];

    public function service () {
        $this->belongsTo('App\Service');
    }

    public function tag () {
        return $this->belongsTo('App\Tag');
    }
}
