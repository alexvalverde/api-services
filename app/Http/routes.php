<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'MainController@index');
Route::get('/services{name?}{description?}{min?}{max?}', ['as' => 'services', 'uses' => 'MainController@index']);

Route::auth();

Route::group(['prefix' => 'api'], function(){

    Route::post('authenticate', 'AuthenticateController@authenticate');
    Route::get('user/logged', 'AuthenticateController@getAuthenticatedUser');
    Route::post('register', ['as' => 'register', 'uses' => 'AuthenticateController@register']);//revisar

    Route::get('/services{name?}{description?}{min?}{max?}', ['as' => 'api.services', 'uses' => 'MainController@index']);
    Route::resource('categories', 'CategoryController', ['only' => 'index']);
    Route::get('/dashboard', 'DashboardController@index');
    Route::group(['prefix' => 'dashboard', 'middleware' => 'jwt.auth', 'jwt.refresh'], function(){
        Route::get('profiles', ['as' => 'dashboard.profiles.index', 'uses' => 'UserProfileController@index']);
        Route::get('profiles/create', ['as' => 'dashboard.profiles.create', 'uses' => 'UserProfileController@create']);
        Route::get('profiles/edit',['as' => 'dashboard.profiles.edit', 'uses' => 'UserProfileController@edit']);
        Route::post('profiles', ['as' => 'dashboard.profiles.store', 'uses' => 'UserProfileController@store']);
        Route::put('profiles',['as' => 'dashboard.profiles.update', 'uses' => 'UserProfileController@update']);
        Route::resource('users.services', 'UserServiceController');
    });
});

