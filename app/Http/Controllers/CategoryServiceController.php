<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoryServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $category = Category::find($id);
        if(!$category){
            return view('404', ['code' => 404, 'message' => 'This user no exist']);
        }
        $services = $category->services()->orderBy('updated_at', 'DESC')->get();

        return response()->json($services, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($user_id)
    {
        $service = new Service();
        $service->user_id = $user_id;
        $categories = Category::all();
        return view('services.create', ['service' => $service, 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        {

            $values = $request->only('user_id', 'category_id', 'name', 'description', 'price');

            $service = Service::create($values);

            //return response()->json(['data' => $service,  'message' => 'Service correctly added', 201]);
            return redirect()->route('dashboard.users.services.index', ['users' => $service->user_id]);
        }

    }


    public function show($user_id, $service_id )
    {
        $user = User::find($user_id);
        if(!$user){
            return view('404', ['code' => 404, 'message' => 'This user no exist']);
        }

        $service = $user->services()->find($service_id);
        if(!$service){
            return view('404', ['code' => 404, 'message' => 'This user no exist']);
        }

        $service->load('category');
        return view('services.show', ['service' => $service]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id, $service_id)
    {
        $user = User::find($user_id);
        if(!$user){
            return view('404', ['code' => 404, 'message' => 'This user no exist']);
        }

        $service = $user->services()->find($service_id);
        if(!$service){
            return view('404', ['code' => 404, 'message' => 'This user no exist']);
        }
        $categories = Category::all();
        return view('services.edit', ['service' => $service, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id, $service_id)
    {
        $user = User::find($user_id);
        if(!$user){
            return view('404', ['code' => 404, 'message' => 'This user no exist']);
        }

        $service = $user->services->find($service_id);
        if(!$service){
            return view('404', ['code' => 404, 'message' => 'This user no exist']);
        }
        $service->user_id = $request->get('user_id');
        $service->category_id = $request->get('category_id');
        $service->name = $request->get('name');
        $service->description = $request->get('description');
        $service->price = $request->get('price');

        $service->save();

        return redirect()->route('dashboard.users.services.index', ['users' => $service->user_id]);
    }


    public function destroy($user_id, $service_id)
    {
        $user = User::find($user_id);
        if(!$user){
            return view('404', ['code' => 404, 'message' => 'This user no exist']);
        }

        $service = $user->services->find($service_id);
        if(!$service){
            return view('404', ['code' => 404, 'message' => 'This user no exist']);
        }

        $service->delete();

        return redirect()->route('dashboard.users.services.index', ['users' => $user_id]);
    }
}
