<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use JWTAuth;
use Illuminate\Support\Facades\Storage;
class UserProfileController extends Controller
{
    public function index (){
        $user = JWTAuth::parseToken()->authenticate();

        if(!$user){
            return response()->json(['message' => 'This User does not exist', 'code' => 404], 404);//code not found
        }
        $profile = $user->profile;
        if(!$profile){
            return response()->json(['message' => 'This User does not have profile', 'code' => 404], 200);//code not found
        }

        return response()->json(['data' => $user->profile,'code' => 200], 200);
    }

    public function create ($id){
        return view('users.profile.create', ['user_id' => $id] );
    }

    public function store(Request $request)
    {
        $profile = new Profile();
        $profile->user_id = $request->get('user_id');
        $profile->first_name = $request->get('first_name');
        $profile->last_name = $request->get('last_name');
        $profile->zip_code = $request->get('zip_code');
        $profile->address = $request->get('address');
        $profile->cellphone = $request->get('cellphone');
        $profile->phone = $request->get('phone');
        $profile->picture =  'user_default.png';
/*
        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $original_name = $request->file('file')->getClientOriginalName();
            $filename = time(). '-' . $original_name;
            $content = file_get_contents($request->file('file')->getRealPath());
            if(Storage::disk('profiles')->put($filename, $content)){
                $profile->picture = $filename;
            }
        }else {
            $profile->picture =  'user_default.png';
        }
*/
        $profile->save();
        return response()->json(['data' => $profile, 'message' => 'Profile correctly added', 'code' => 201], 201); //code Created
    }

    public function edit (){
        $id = Auth::user()->id;
        $user = User::find($id);

        if(!$user)
        {
            return response()->json(['message' => 'This User does not exist', 'code' => 404], 404);//code not found
        }

        $profile = $user->profile;

        if(!$profile)
        {
            $profile = new Profile();
            $profile->user_id = $id;
            return view('users.profile.create', ['profile' => $profile]);
        }
        return view('users.profile.edit', ['profile' => $profile]);
    }


    public function update(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if(!$user)
        {
            return response()->json(['message' => 'This User does not exist', 'code' => 404], 404);//code not found
        }

        $profile = $user->profile;

        if(!$profile)
        {
            return response()->json(['message' => 'This Profile does not exist', 'code' => 404], 404);//code not found
        }

        $profile->user_id = $request->get('user_id');
        $profile->first_name = $request->get('first_name');
        $profile->last_name = $request->get('last_name');
        $profile->zip_code = $request->get('zip_code');
        $profile->address = $request->get('address');
        $profile->cellphone = $request->get('cellphone');
        $profile->phone = $request->get('phone');
        $profile->picture =  'user_default.png';
/*
        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $original_name = $request->file('file')->getClientOriginalName();
            $exists = Storage::disk('profiles')->exists($original_name);
            if($exists){
                Storage::delete($original_name);
            }
            $filename = time(). '-' . $original_name;
            $content = file_get_contents($request->file('file')->getRealPath());
            if(Storage::disk('profiles')->put($filename, $content)){
                $profile->picture = $filename;
            }
        }else {
            $profile->picture =  'user_default.png';
        }
*/

        $profile->save();
        return response()->json(['data' => $profile, 'message' => 'Profile correctly updated', 'code' => 200], 200);//code Ok
    }
}
