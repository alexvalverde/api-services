<?php

namespace App\Http\Controllers;

use App\Category;
use App\Service;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $user = User::find($id);
        if(!$user){
            return response()->json(['code' => 404, 'message' => 'This user no exist'], 404);
        }

        $pageSize =  $request->get('pageSize')  ?  $request->get('pageSize') : 10;
        $orderBy =  $request->get('sort')  ?  json_decode($request->get('sort')): collect(['property' => 'updated_at', 'dir' => 'DESC' ]);
        $services = $user->services()->orderBy($orderBy->property, $orderBy->dir)->paginate($pageSize);
        $services->load('category');
        return response()->json($services, 200);
    }

    public function store(Request $request)
    {

        $values = $request->only('user_id', 'category_id', 'name', 'description', 'price');

        $service = Service::create($values);

        return response()->json(['data'=> $service], 201);

    }


    public function show($user_id, $service_id )
    {
        $user = User::find($user_id);
        if(!$user){
            return response()->json(['code' => 404, 'message' => 'This user no exist'], 404);
        }

        $service = $user->services->find($service_id);
        if(!$service){
            return response()->json(['code' => 404, 'message' => 'This service no exist'], 404);
        }
        $service->load('category');

        return  response()->json(['data'=>$service]);
    }


    public function update(Request $request, $user_id, $service_id)
    {
        $user = User::find($user_id);
        if(!$user){
            return response()->json(['code' => 404, 'message' => 'This user no exist'], 404);
        }

        $service = $user->services->find($service_id);

        if(!$service){
            return response()->json(['code' => 404, 'message' => 'This service no exist'], 404);
        }
        $service->user_id = $request->get('user_id');
        $service->category_id = $request->get('category_id');
        $service->name = $request->get('name');
        $service->description = $request->get('description');
        $service->price = $request->get('price');

        $service->save();

        return response()->json(['data'=> $service], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id, $service_id)
    {
        $user = User::find($user_id);
        if(!$user){
            return response()->json(['code' => 404, 'message' => 'This user no exist'], 404);
        }

        $service = $user->services->find($service_id);

        if(!$service){
            return response()->json(['code' => 404, 'message' => 'This service no exist'], 404);
        }

        $service->delete();

        return redirect()->route('dashboard.users.services.index', ['users' => $user_id]);
    }
}
