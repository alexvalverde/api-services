<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

use App\Http\Requests;

class MainController extends Controller
{
    public function index (Request $request){
        $services = Service::with(
            [
                'user' => function ($query) {
                    return $query->select('id', 'email');
                },
                'user.profile' => function ($query){
                    return $query->select('user_id', 'phone', 'cellphone');
                },
                'category',
                'tags'
            ]
        );
        if($request->get('name')){
            $services =  $services->where('name', 'like', '%'.$request->get('name').'%');
        }
        if($request->get('category_id')){
            $services =  $services->where('category_id', '=', $request->get('category_id'));
        }

        if($request->get('description')){
            $services =  $services->where('description', 'like', '%'.$request->get('description').'%');
        }

        if($request->get('min')){
            $services =  $services->where('price', '>=', $request->get('min'));
        }

        if($request->get('max')){
            $services =  $services->where('price', '<=', $request->get('max'));
        }
        $services =  $services->paginate($request->get('pageSize'));
        return response()->json($services, 200);
    }
}
