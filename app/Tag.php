<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';

    protected $fillable = ['tag'];

    public function services () {
        return $this->belongsToMany('App\Services', 'service_tags', 'tag_id', 'service_id');
    }


}
