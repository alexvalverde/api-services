<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('categories')->delete();

        for( $i = 1 ; $i <= 4 ; $i++){
            Category::create(['name' => 'Category-'.$i]);
        }

        Model::reguard();
    }
}

