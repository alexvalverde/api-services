<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Profile;
use App\User;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('profiles')->delete();
        $users =  User::all();
        $faker = Faker\Factory::create('es_ES');
        foreach($users as $user){
            $profile = [
                'user_id' => $user->id,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'phone' => $faker->phoneNumber,
                'zip_code' => $faker->postcode,
                'cellphone'=>$faker->phoneNumber,
                'picture'=> 'user_default.jpg' ,
                'address' => $faker->address
            ];
            Profile::create($profile);
        }

        Model::reguard();
    }
}
