<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('tags')->delete();

        for( $i = 1 ; $i <= 10 ; $i++){
            Tag::create(['tag' => 'tag-'.$i]);
        }

        Model::reguard();
    }
}
