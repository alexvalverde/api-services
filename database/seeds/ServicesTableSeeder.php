<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Category;
use App\Service;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('services')->delete();
        $faker = Faker\Factory::create('es_ES');

        $users = User::all();
        $categories = Category::all();
        $i = 1;
        foreach($users as $user){
            foreach ($categories as $category) {
                Service::create([
                    'category_id' => $category->id,
                    'user_id' => $user->id,
                    'name' => 'Service-'.$i,
                    'description' => $faker->sentences(3, true),
                    'price' => $faker->randomFloat(2, 1, 100)
                ]);
                $i++;
            }
        }
        Model::reguard();
    }
}